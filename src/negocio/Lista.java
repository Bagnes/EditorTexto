package negocio;
import java.util.ArrayList;

public class Lista
{
	private ArrayList<String> personas;
	
	public Lista()
	{
		personas = new ArrayList<String>();
	}
	
	public void agregar(String p)
	{
		personas.add(p);
	}
	
	public int cantidad()
	{
		return personas.size();
	}
	
	public String consultarPersona(int i)
	{
		return personas.get(i);
	}
	
	public Memento getMemento()
	{
		return new Memento(personas);
	}
	
	public void restore(Memento m)
	{
		personas = m.getArray();
	}
}
