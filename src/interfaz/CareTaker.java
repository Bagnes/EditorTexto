package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import negocio.Lista;
import negocio.Memento;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.LinkedList;

import javax.swing.JScrollPane;

public class CareTaker
{
	private Lista lista;
	private LinkedList<Memento> mementos;
	
	private JFrame frame;
	private JTextField textNombre;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton btnDeshacer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CareTaker window = new CareTaker();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CareTaker()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e)
		{
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		lista = new Lista();
		mementos = new LinkedList<Memento>();
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIngreseSuNombre = new JLabel("Nombre:");
		lblIngreseSuNombre.setBounds(40, 31, 86, 14);
		frame.getContentPane().add(lblIngreseSuNombre);
		
		textNombre = new JTextField();
		textNombre.setBounds(112, 28, 86, 20);
		frame.getContentPane().add(textNombre);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				String p = textNombre.getText();
				if( p.length() > 0 )
				{
					mementos.add( lista.getMemento() );
					lista.agregar(p);
					mostrarLista();
				}
			}
		});
		btnAgregar.setBounds(222, 27, 89, 23);
		frame.getContentPane().add(btnAgregar);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 70, 395, 171);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		btnDeshacer = new JButton("Deshacer");
		btnDeshacer.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				deshacer();
			}
		});
		btnDeshacer.setBounds(321, 27, 89, 23);
		frame.getContentPane().add(btnDeshacer);
	}
	
	private void deshacer() 
	{
		if( mementos.size() > 0 )
		{
			lista.restore( mementos.getLast() );
			mementos.removeLast();
			mostrarLista();
		}
	}
	
	public void mostrarLista()
	{
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Nombre");
		
		for(int i=0; i<lista.cantidad(); ++i)
		{
			String p = lista.consultarPersona(i);
			model.addRow(new String[] { p });
		}
		
		table.setModel(model);
	}
}
